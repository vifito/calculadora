package eu.vifito.calculadora.controllers;

import java.util.Collections;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SumaController {

  @RequestMapping("/sumar/{a}/{b}")
  public Map<String, Integer> sumar(@PathVariable Integer a, @PathVariable Integer b) {
    return Collections.singletonMap("response", a+b);
  }

}
